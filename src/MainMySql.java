import java.io.*;
import java.sql.*;

public class MainMySql {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String args[]) {
        listarDatos();
        insertarDatos();
        listarDatos();
        eliminarDatos();
        listarDatos();
        actualizarDatos();
        listarDatos();
    }

    public static void listarDatos() {
        try {
            Connection conn = null;
            String query = "SELECT * FROM PERSONA";
            Statement stmt = null;
            ResultSet rs = null;
            String strConexion = "jdbc:mysql://localhost/PersonaBD?user=root&password=1qaz2wsx3edc&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);// el rs almacena la información de la base de datos.
            // TENGO QUE RECORRERLA
            while (rs.next()) { //rs.next devuelve true si hay más líneas en el result set. por defecto, al iniciar el ciclo, el rs está en la línea 0.
                System.out.println(rs.getInt("CEDULA") + " - " + rs.getString("NOMBRE"));
            }
            stmt.close();
            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void insertarDatos() {
        try {

            System.out.print("Ingrese la cedula de la persona: ");
            int cedula = Integer.parseInt(in.readLine());
            System.out.print("Ingrese el nombre de la persona: ");
            String nombre = in.readLine();

            Connection conn = null;
            String query = "INSERT INTO PERSONA VALUES ("+cedula+",'"+nombre+"')";
            Statement stmt = null;
            String strConexion = "jdbc:mysql://localhost/PersonaBD?user=root&password=1qaz2wsx3edc&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            stmt.executeUpdate(query); // Nos sirve para Insertar, Actualizar, Eliminar
            stmt.close();
            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void eliminarDatos(){
        try {
            System.out.print("Ingrese la cedula de la persona: ");
            int cedula = Integer.parseInt(in.readLine());

            Connection conn = null;
            String query = "DELETE FROM PERSONA WHERE CEDULA="+cedula+"";
            Statement stmt = null;
            String strConexion = "jdbc:mysql://localhost/PersonaBD?user=root&password=1qaz2wsx3edc&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            stmt.executeUpdate(query); // Nos sirve para Insertar, Actualizar, Eliminar
            stmt.close();
            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void actualizarDatos(){
        try {
            System.out.print("Ingrese la cedula de la persona: ");
            int cedula = Integer.parseInt(in.readLine());
            System.out.print("Ingrese el nombre de la persona: ");
            String nombre = in.readLine();

            Connection conn = null;
            String query = "UPDATE PERSONA SET NOMBRE='"+nombre+"' WHERE CEDULA = "+cedula+"";
            Statement stmt = null;
            String strConexion = "jdbc:mysql://localhost/PersonaBD?user=root&password=1qaz2wsx3edc&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
            conn = DriverManager.getConnection(strConexion);
            stmt = conn.createStatement();
            stmt.executeUpdate(query); // Nos sirve para Insertar, Actualizar, Eliminar
            stmt.close();
            conn.close();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
